FROM node:10 AS build 

WORKDIR /app

COPY package.json .
RUN npm install

COPY . . 
RUN npm run-script build

FROM nginx:latest
COPY --from=build /app/static/ /usr/share/nginx/html/static/
COPY --from=build /app/build/ /usr/share/nginx/html/
